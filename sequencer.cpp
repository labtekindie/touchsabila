/* sequencer.c
 * TouchSabila
 *
 * (c) 2013 Tuwuh Sarwoprasojo, Labtek Indie
 * All rights reserved.
 */

#include "sequencer.h"

Sequencer::Sequencer(QObject *parent) :
    QObject(parent)
{
    for (int i=0; i<12; i++) {
        QString s;
        s.sprintf("sound/%02d.wav", i+1);
        qDebug() << s;
        for (int j=0; j<16; j++) {
            m_sound[j][i].setSource(QUrl::fromLocalFile(s));
        }
    }

    clearPads();
    m_step = 0;
    QObject::connect(&m_timer, SIGNAL(timeout()), this, SLOT(handleTimeout()));
    m_timer.start(125);
}

void Sequencer::setPad(int col, int row, bool value)
{
    m_pads[col][row] = value;
}

void Sequencer::clearPads()
{
    for (int i=0; i<16; i++) {
        for (int j=0; j<12; j++) {
            m_pads[i][j] = false;
        }
    }
}

void Sequencer::printPads()
{
    QString s;
    for (int j=0; j<12; j++) {
        for (int i=0; i<16; i++) {
            if (m_pads[i][j]) {
                s += "1";
            } else {
                s += "0";
            }
        }
        s += "\n";
    }
    qDebug() << qPrintable(s);

}

void Sequencer::playStep()
{
    for (int i=0; i<12; i++) {
        if (m_pads[m_step][i]) {
            m_sound[m_step][i].play();
        }
    }
}

void Sequencer::printStep(bool padStep[])
{
    QString s;
    for (int i=0; i<12; i++) {
        if (padStep[i]) {
            s += "1";
        } else {
            s += "0";
        }
    }

    qDebug() << qPrintable(s);
}

void Sequencer::handleTimeout()
{
    m_step = (m_step + 1) % 16;
    playStep();
    emit stepChanged(m_step, m_pads[m_step]);
}


