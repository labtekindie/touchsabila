/* sequencer.h
 * TouchSabila
 *
 * (c) 2013 Tuwuh Sarwoprasojo, Labtek Indie
 * All rights reserved.
 */

#ifndef SEQUENCER_H
#define SEQUENCER_H

#include <QtCore>
#include <QtMultimedia/QSoundEffect>

class Sequencer : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int step READ step WRITE setStep NOTIFY stepChanged)

public:
    explicit Sequencer(QObject *parent = 0);

    int step() const {
        return m_step;
    }

    void setStep(int step) {
        m_step = step;
        emit stepChanged(m_step, m_pads[m_step]);
    }

    Q_INVOKABLE void setPad(int row, int col, bool value);
    Q_INVOKABLE void clearPads();
    void printPads();

    void playStep();

    void printStep(bool padStep[]);

signals:
    void stepChanged(int step, bool padStep[]);

public slots:
    void handleTimeout();

private:
    QTimer m_timer;
    int m_step;
    bool m_pads[16][12];
    QSoundEffect m_sound[16][12];
};

#endif // SEQUENCER_H
