#include <QtGui/QGuiApplication>
#include <QtQml>
#include <QtMultimedia/QSoundEffect>
#include "qtquick2applicationviewer.h"
#include "sequencer.h"

int main(int argc, char *argv[])
{
    QGuiApplication app(argc, argv);

    //qmlRegisterType<Sequencer>("com.labtekindie.Sequencer", 1, 0, "Sequencer");

    QtQuick2ApplicationViewer viewer;

    Sequencer *sequencer = new Sequencer();
    viewer.rootContext()->setContextProperty("sequencer", sequencer);

    viewer.setMainQmlFile(QStringLiteral("qml/touchsabila/main.qml"));
    viewer.setWidth(800);
    viewer.setHeight(600);
    //viewer.showFullScreen();
    viewer.showExpanded();

    return app.exec();
}
